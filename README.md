# Phase 2 Starting Project

This is a base project for starting phase 2.

It contains a Gradle multi-project similar to the one used in lab 9.

## The Sub-Projects

* `common` --- Domain classes, and logback configuration.
* `customer-ajax` --- The AJAX client for creating customer accounts.  This is based on the AngularJS starting project from lab 3.
* `customer-service` --- The customer accounts service project from phase 1.
* `sales-service` --- The sales service from phase 1.
* `mock-mail` --- A mock email server that you can use for testing.
* `router` --- The router project.

These projects all have the appropriate libraries added to them.

If needed, you can run the test clients directly from your phase 1 project.  There is no need to add them to this project.

### Migrating the Phase 1 Projects

It will make things easier for you to have your phase 1 projects in the same multi-project as the phase 2 projects.  Multi-projects are easier to work with and you will need to be able to share the domain classes across several projects.

Copy the `src/main` folder from each of your phase 1 service projects into the appropriate `src` folder of the phase 2 project.  If you have unit tests in your phase 1 service projects then also copy the `src/tests` folders.

#### Main Classes

The `build.gradle` file for each of the two phase 1 services projects has a `mainClassName` attribute that is hard-coded to `server.Server`.  If the name of your main classes in your phase 1 services do not match this then change the `mainClassName` attribute (at the bottom of each file) in the `build.gradle` to match your server class name.

This means that you can run a service by right clicking the sub-project --- you don't even need to have it open.  This is much faster than poking around in your various projects looking for the main classes.

#### Domain Classes

There are two `Customer` domain classes --- one in each of the two services.  You should refactor/rename the customer accounts  service version of the class to `CustomerAccount` so that it is easy to tell which customer class is which.

The domain classes need to be shared between the service and router projects.  To avoid having multiple copies of the domain classes you should move the domain classes from the service projects into the `src/main/java` project of the root project.  Note that you can drag and drop files/folders directly into the project pane in NetBeans.

#### Test the Phase 1 Projects

Your phase 1 projects should still function properly after this migration.  Run and test your services to verify that everything works properly.  Complete this migration early so that you can get some help if it doesn't work for you.
