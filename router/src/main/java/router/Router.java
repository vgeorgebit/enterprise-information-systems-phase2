/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package router;

import builder.AccRouteBuilder;
import builder.SaleRouteBuilder;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.camel.CamelContext;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.jms.JmsComponent;
import org.apache.camel.impl.DefaultCamelContext;

/**
 *
 * @author vg
 */
public class Router {

    public static void main(String[] args) throws Exception {
        // create default context
        CamelContext camel = new DefaultCamelContext();

        // register ActiveMQ as the JMS handler
        ActiveMQConnectionFactory activeMqFactory = new ActiveMQConnectionFactory("tcp://localhost:61616");
        JmsComponent jmsComponent = JmsComponent.jmsComponent(activeMqFactory);
        camel.addComponent("jms", jmsComponent);

        // transfer the entire exchange, or just the body and headers?
        jmsComponent.setTransferExchange(true); // changed to true to keep properties in order to store bits of messages to keep track of

        // trust all classes being used to send serialised domain objects
        activeMqFactory.setTrustAllPackages(true);

        // turn exchange tracing on or off (false is off)
        camel.setTracing(false);

        // enable stream caching so that things like loggers don't consume the messages
        camel.setStreamCaching(true);

        // create and add the builder(s)
        camel.addRoutes(new AccRouteBuilder()); // for the account route builder
        camel.addRoutes(new SaleRouteBuilder()); // for the sale route builder

        // start routing
        System.out.println("Starting router...");
        camel.start();
        System.out.println("... ready. Press enter to shutdown.");
        System.in.read();
        camel.stop();
        
        // create message producer
        ProducerTemplate producer = camel.createProducerTemplate();

        // context must be started before we can send messages
        camel.start();

        // send a message
        producer.sendBody("jms:queue:somewhere", "message to send");

    }

}
