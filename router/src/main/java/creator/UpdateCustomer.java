/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package creator;

import model.CustomerAccount;

/**
 *
 * @author vg
 */
public class UpdateCustomer {

    public CustomerAccount updateCustomer(String email, String firstName, String lastName, String group) {
        
        CustomerAccount custAcc = new CustomerAccount();
        
        custAcc.setEmail(email);
        custAcc.setFirstName(firstName);
        custAcc.setLastName(lastName);
        custAcc.setGroup(group);
        
        return custAcc;

    }
}
