/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package builder;

import creator.CustomerCreator;
import model.CustomerAccount;
import org.apache.camel.Exchange;
import org.apache.camel.ExchangePattern;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;

/**
 *
 * @author vg
 */
public class AccRouteBuilder extends RouteBuilder {

    @Override
    public void configure() {

        // An embedded instance of Jetty
        from("jetty:http://localhost:9000/customer?enableCORS=true")
                .setExchangePattern(ExchangePattern.InOnly) // this is necessary as we don't have a reply message being created and we don't want our client waiting forever
                .unmarshal().json(JsonLibrary.Gson, CustomerAccount.class)
                .process(exchange -> {
                    CustomerAccount custAcc = exchange.getIn().getBody(CustomerAccount.class);
                    custAcc.setGroup("0afa8de1-147c-11e8-edec-2b197906d816");
                })
                .log("${body}")
                .marshal().json(JsonLibrary.Gson) //marshall to JSON
                .multicast().to("jms:queue:new_customer_service", "jms:queue:send-to-vend");
        
        // route to handle creating a new customer for customer service
        from("jms:queue:new_customer_service")
                .removeHeaders("*")
                .setHeader("Content-Type").simple("application/json")
                .setHeader(Exchange.HTTP_METHOD,constant("POST"))
                .log("${body}")
                .to("jetty:http://localhost:8080/api/customers");
        
        // route to handle creating a new customer for vend
        from("jms:queue:send-to-vend")
                .setExchangePattern(ExchangePattern.InOnly)
                .unmarshal().json(JsonLibrary.Gson, CustomerAccount.class)
                .bean(CustomerCreator.class, "createCustomer(${body.firstName},${body.lastName},${body.group},${body.email})")
                // remove headers so they don't get sent to Vend
                .removeHeaders("*")
                // add authentication token to authorization header
                .setHeader("Authorization", constant("Bearer KiQSsELLtocyS2WDN5w5s_jYaBpXa0h2ex1mep1a"))
                // marshal to JSON
                .marshal().json(JsonLibrary.Gson) // only necessary if the message is an object, not JSON
                .setHeader(Exchange.CONTENT_TYPE).constant("application/json")
                // set HTTP method
                .setHeader(Exchange.HTTP_METHOD, constant("POST"))
                // send it
                .log("${body}")
                .to("https://info303otago.vendhq.com/api/2.0/customers")
                // store the response
                .to("jms:queue:vend-response");

    }

}
