/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package builder;

import calculator.GroupCalculator;
import creator.CustomerCreator;
import model.CustomerAccount;
import model.Sale;
import model.Summary;
import org.apache.camel.Exchange;
import org.apache.camel.ExchangePattern;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;

/**
 *
 * @author vg
 */
public class SaleRouteBuilder extends RouteBuilder {

    @Override
    public void configure() {

        from("imap://localhost?username=test@localhost"
                + "&port=3143"
                + "&password=password"
                + "&consumer.delay=5000"
                + "&searchTerm.subject=Vend:SaleUpdate")// matches mock sale email subject header
                .log("Found new E-Mail: ${body}")
                .to("jms:queue:new_sale_customer");
 
        // capture the email properties    
        from("jms:queue:new_sale_customer")
                .setExchangePattern(ExchangePattern.InOnly)// client wont be waiting for a response
                .unmarshal().json(JsonLibrary.Gson, Sale.class)
                .setProperty("customer_id").simple("${body.customer.id}") //set the customer ID property
                .setProperty("email").simple("${body.customer.email}")
                .setProperty("first_Name").simple("${body.customer.firstName}")
                .setProperty("last_Name").simple("${body.customer.lastName}")
                .setProperty("customer_group_id").simple("${body.customer.group}")
                .log("${body}")
                .marshal().json(JsonLibrary.Gson)
                .to("jms:queue:new_sale_customer_service");
        
        from("jms:queue:new_sale_customer_service")
                .setHeader(Exchange.HTTP_METHOD, constant("POST"))
                .setHeader(Exchange.CONTENT_TYPE, constant("application/json"))
                .to("http://localhost:8081/api/sales")
                .removeHeaders("*")
                .setBody(constant(null))// Get requests are not meant to have bodies 
                .setHeader(Exchange.HTTP_METHOD, constant("GET"))
                .setHeader(Exchange.CONTENT_TYPE, constant("application/json"))
                .toD("http://localhost:8081/api/sales/customer/${exchangeProperty.customer_id}/summary") // create dynamic URI Endpoint
                .unmarshal().json(JsonLibrary.Gson, Summary.class)
                .setProperty("calculateGroup").method(GroupCalculator.class, "calculateGroup(${body.group})") // extracting calculated group from Summary
                .log("${exchangeProperty.email}")
                .choice()
                    .when().simple("${exchangeProperty.customer_group_id} != ${exchangeProperty.calculateGroup}")
                        .removeHeaders("*")
                        .setHeader(Exchange.HTTP_METHOD, constant("PUT"))
                        .setHeader(Exchange.CONTENT_TYPE, constant("application/json"))
                        .bean(CustomerCreator.class, "updateCustomer(${exchangeProperty.email},${exchangeProperty.first_Name},${exchangeProperty.last_Name},${exchangeProperty.calculateGroup})")
                        .marshal().json(JsonLibrary.Gson)
                        .log("${body}")
                        .log("${exchangeProperty.email}")
                        .toD("http://localhost:8080/api/customers/customer/${exchangeProperty.email}")
                        .unmarshal().json(JsonLibrary.Gson, CustomerAccount.class)
                        .bean(CustomerCreator.class, "createCustomer(${exchangeProperty.first_Name},${exchangeProperty.last_Name},${exchangeProperty.calculateGroup})")
                        //.log("${exchangeProperty.calculateGroup}")
                        .marshal().json(JsonLibrary.Gson) //Marshall to JSON
                        .to("jms:queue:vend_customer_update")
                .end();
        
            from("jms:queue:vend_customer_update")
                    .removeHeaders("*")
                    // add authentication token to authorization header
                    .setHeader("Authorization", constant("Bearer KiQSsELLtocyS2WDN5w5s_jYaBpXa0h2ex1mep1a"))
                    // set HTTP method
                    .setHeader(Exchange.HTTP_METHOD, constant("PUT"))
                    .setHeader(Exchange.CONTENT_TYPE, constant("application/json"))                
                    // send it
                    .toD("https://info303otago.vendhq.com/api/2.0/customers/${exchangeProperty.customer_id}")
                    .log("${exchangeProperty.customer_id}")
                    .to("jms:queue:vend-response");
 
    }

}
