package dao;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import model.CustomerAccount;

public class CustomerDAO {

	private static final Map<String, CustomerAccount> customers = new TreeMap<>();

	public List<CustomerAccount> getAll() {
		return new ArrayList<>(customers.values());
	}

	public void save(CustomerAccount customer) {
		customers.put(customer.getEmail(), customer);
	}

	public CustomerAccount get(String emailAddress) {
		return customers.get(emailAddress);
	}

	public void delete(String emailAddress) {
		customers.remove(emailAddress);
	}

	public void update(String emailAddress, CustomerAccount updatedCustomer) {
		customers.put(emailAddress, updatedCustomer);
	}

	public boolean exists(String emailAddress) {
		return customers.containsKey(emailAddress);
	}

}
