package service;

import dao.CustomerDAO;
import java.io.IOException;
import java.util.concurrent.CompletableFuture;
import org.jooby.Jooby;
import org.jooby.apitool.ApiTool;
import org.jooby.json.Gzon;
import resource.CustomerResource;
import resource.CustomersResource;

public class Service extends Jooby {

	public Service() {

		CustomerDAO dao = new CustomerDAO();

		port(8080);

		use(new Gzon());

		use(new CustomersResource(dao));
		use(new CustomerResource(dao));

		use(new ApiTool().swagger(new ApiTool.Options("/swagger").use("accounts.yaml")));

	}

	public static void main(String[] args) throws IOException {

		Service server = new Service();

		CompletableFuture.runAsync(() -> {
			server.start();
		});

		server.onStarted(() -> {
			System.out.println("\nPress Enter to stop service.");
		});

		System.in.read();
		System.exit(0);
	}

}
