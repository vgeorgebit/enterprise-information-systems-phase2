package resource;

import dao.CustomerDAO;
import model.CustomerAccount;
import model.ErrorMessage;
import org.jooby.Jooby;
import org.jooby.MediaType;
import org.jooby.Status;

public class CustomersResource extends Jooby {

	public CustomersResource(CustomerDAO dao) {

		path("/api/customers", () -> {

			get(() -> {
				return dao.getAll();
			});

			post((req, rsp) -> {

				CustomerAccount customer = req.body(CustomerAccount.class);

				String uri = req.path() + "/customer/" + customer.getUsername();

				customer.setUri(uri);

				if (dao.exists(customer.getEmail())) {
					rsp.status(Status.UNPROCESSABLE_ENTITY).send(new ErrorMessage("There is already a customer with that email address."));
				} else {
					dao.save(customer);
					rsp.status(Status.CREATED).send(customer);
				}

			});

		}).produces(MediaType.json).consumes(MediaType.json);

	}

}
