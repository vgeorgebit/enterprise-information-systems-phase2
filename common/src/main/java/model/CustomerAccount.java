package model;

public class CustomerAccount implements Comparable<CustomerAccount>{

	private String email;
	private String username;
	private String firstName;
	private String lastName;
	private String group;
	private String uri;

	public CustomerAccount() {
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getGroup() {
		return group;
	}

	public void setGroup(String group) {
		this.group = group;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	@Override
	public String toString() {
		return "Customer{" + "username=" + username + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email + ", group=" + group + ", uri=" + uri + '}';
	}

	@Override
	public int compareTo(CustomerAccount other) {
		return this.email.compareTo(other.getEmail());
	}

}
