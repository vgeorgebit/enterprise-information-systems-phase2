/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

"use strict";
let module = angular.module('AccountModule', ['ngResource']);
module.controller('AccountController', function () {
});

    
    // creating a template for the API
    module.factory('newCustApi', function ($resource) {
        return $resource('http://localhost:9000/customer', null);
    });
    
    module.controller('AccountController',['$scope', 'newCustApi', function($scope,newCustApi) {
        $scope.formSubmitNewCust = function () {
            const customer = {
                firstName: $scope.firstName,
                lastName: $scope.lastName,
                username: $scope.username,
                email: $scope.email
            };
             newCustApi.save(customer);   
             //alert("Capture customer details")
             //console.log(customer);
        };
    }]);

