import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.GreenMailUtil;
import com.icegreen.greenmail.util.ServerSetupTest;
import java.io.IOException;


public class MockMailServer {

	public static void main(String[] args) throws IOException {
		String subject = "Vend:SaleUpdate";

                String body = "{\"created_at\":\"2019-09-26 14:54:37\",\"customer\":{\"balance\":\"0.00000\",\"company_name\":null,\"contact_first_name\":\"Aaaaaaa\",\"contact_last_name\":\"Aaaaaaa\",\"created_at\":\"2019-09-26 14:54:06\",\"custom_field_1\":null,\"custom_field_2\":null,\"custom_field_3\":null,\"custom_field_4\":null,\"customer_code\":\"Aaaaaaa-K94E\",\"customer_group_id\":\"0afa8de1-147c-11e8-edec-2b197906d816\",\"date_of_birth\":null,\"deleted_at\":null,\"do_not_email\":false,\"email\":\"doris@example.net\",\"enable_loyalty\":true,\"fax\":null,\"first_name\":\"Aaaaaaa\",\"id\":\"06c2f1bf-e97c-11e9-efcf-e06d7d2ea00b\",\"last_name\":\"Aaaaaaa\",\"loyalty_balance\":\"0.00000\",\"mobile\":null,\"note\":null,\"phone\":null,\"points\":0,\"sex\":null,\"updated_at\":\"2019-09-26 14:54:37\",\"year_to_date\":\"758.09998\"},\"customer_id\":\"06c2f1bf-e97c-11e9-efcf-e06d7d2ea00b\",\"deleted_at\":null,\"id\":\"b78e2cbc-ea0d-9af5-11e9-e06d865a9eaa\",\"invoice_number\":\"20\",\"note\":\"\",\"outlet_id\":\"06c2f1bf-e97c-11e9-efcf-bc24f33dbae1\",\"register_id\":\"06c2f1bf-e97c-11e9-efcf-bc2502e0c9e4\",\"register_sale_payments\":[{\"amount\":758.1,\"id\":\"b78e2cbc-ea0d-9af5-11e9-e06d8e367dfa\",\"payment_date\":\"2019-09-26T14:54:35Z\",\"payment_type\":{\"has_native_support\":false,\"id\":\"3\",\"name\":\"Credit Card\"},\"payment_type_id\":3,\"retailer_payment_type\":{\"config\":null,\"id\":\"0afa8de1-1450-11e8-edec-056e6ed079b9\",\"name\":\"Credit Card\",\"payment_type_id\":\"3\"},\"retailer_payment_type_id\":\"0afa8de1-1450-11e8-edec-056e6ed079b9\"}],\"register_sale_products\":[{\"discount\":\"0.00000\",\"id\":\"b78e2cbc-ea0d-9af5-11e9-e06d8add409c\",\"is_return\":false,\"loyalty_value\":\"0.00000\",\"note\":null,\"price\":\"60.78261\",\"price_set\":false,\"price_total\":\"60.78261\",\"product_id\":\"0afa8de1-147c-11e8-edec-056e701f4190\",\"quantity\":1,\"tax\":\"9.11739\",\"tax_id\":\"0afa8de1-1450-11e8-edec-056e6ec70277\",\"tax_total\":\"9.11739\"},{\"discount\":\"0.00000\",\"id\":\"b78e2cbc-ea0d-9af5-11e9-e06d8b97b6ff\",\"is_return\":false,\"loyalty_value\":\"0.00000\",\"note\":null,\"price\":\"172.95652\",\"price_set\":false,\"price_total\":\"172.95652\",\"product_id\":\"0afa8de1-147c-11e8-edec-056e70590621\",\"quantity\":1,\"tax\":\"25.94348\",\"tax_id\":\"0afa8de1-1450-11e8-edec-056e6ec70277\",\"tax_total\":\"25.94348\"},{\"discount\":\"0.00000\",\"id\":\"b78e2cbc-ea0d-9af5-11e9-e06d8bef73f3\",\"is_return\":false,\"loyalty_value\":\"0.00000\",\"note\":null,\"price\":\"60.78261\",\"price_set\":false,\"price_total\":\"60.78261\",\"product_id\":\"0afa8de1-147c-11e8-edec-056e701f4190\",\"quantity\":1,\"tax\":\"9.11739\",\"tax_id\":\"0afa8de1-1450-11e8-edec-056e6ec70277\",\"tax_total\":\"9.11739\"},{\"discount\":\"0.00000\",\"id\":\"b78e2cbc-ea0d-9af5-11e9-e06d8c040d68\",\"is_return\":false,\"loyalty_value\":\"0.00000\",\"note\":null,\"price\":\"60.78261\",\"price_set\":false,\"price_total\":\"60.78261\",\"product_id\":\"0afa8de1-147c-11e8-edec-056e701f4190\",\"quantity\":1,\"tax\":\"9.11739\",\"tax_id\":\"0afa8de1-1450-11e8-edec-056e6ec70277\",\"tax_total\":\"9.11739\"},{\"discount\":\"0.00000\",\"id\":\"b78e2cbc-ea0d-9af5-11e9-e06d8c1a2d7e\",\"is_return\":false,\"loyalty_value\":\"0.00000\",\"note\":null,\"price\":\"60.78261\",\"price_set\":false,\"price_total\":\"60.78261\",\"product_id\":\"0afa8de1-147c-11e8-edec-056e701f4190\",\"quantity\":1,\"tax\":\"9.11739\",\"tax_id\":\"0afa8de1-1450-11e8-edec-056e6ec70277\",\"tax_total\":\"9.11739\"},{\"discount\":\"0.00000\",\"id\":\"b78e2cbc-ea0d-9af5-11e9-e06d8c2eee05\",\"is_return\":false,\"loyalty_value\":\"0.00000\",\"note\":null,\"price\":\"60.78261\",\"price_set\":false,\"price_total\":\"60.78261\",\"product_id\":\"0afa8de1-147c-11e8-edec-056e701f4190\",\"quantity\":1,\"tax\":\"9.11739\",\"tax_id\":\"0afa8de1-1450-11e8-edec-056e6ec70277\",\"tax_total\":\"9.11739\"},{\"discount\":\"0.00000\",\"id\":\"b78e2cbc-ea0d-9af5-11e9-e06d8c4730fd\",\"is_return\":false,\"loyalty_value\":\"0.00000\",\"note\":null,\"price\":\"60.78261\",\"price_set\":false,\"price_total\":\"60.78261\",\"product_id\":\"0afa8de1-147c-11e8-edec-056e701f4190\",\"quantity\":1,\"tax\":\"9.11739\",\"tax_id\":\"0afa8de1-1450-11e8-edec-056e6ec70277\",\"tax_total\":\"9.11739\"},{\"discount\":\"0.00000\",\"id\":\"b78e2cbc-ea0d-9af5-11e9-e06d8c5d9f36\",\"is_return\":false,\"loyalty_value\":\"0.00000\",\"note\":null,\"price\":\"60.78261\",\"price_set\":false,\"price_total\":\"60.78261\",\"product_id\":\"0afa8de1-147c-11e8-edec-056e701f4190\",\"quantity\":1,\"tax\":\"9.11739\",\"tax_id\":\"0afa8de1-1450-11e8-edec-056e6ec70277\",\"tax_total\":\"9.11739\"},{\"discount\":\"0.00000\",\"id\":\"b78e2cbc-ea0d-9af5-11e9-e06d8c73bf50\",\"is_return\":false,\"loyalty_value\":\"0.00000\",\"note\":null,\"price\":\"60.78261\",\"price_set\":false,\"price_total\":\"60.78261\",\"product_id\":\"0afa8de1-147c-11e8-edec-056e701f4190\",\"quantity\":1,\"tax\":\"9.11739\",\"tax_id\":\"0afa8de1-1450-11e8-edec-056e6ec70277\",\"tax_total\":\"9.11739\"}],\"return_for\":null,\"sale_date\":\"2019-09-26T14:54:35Z\",\"short_code\":\"bkzcmu\",\"source\":\"USER\",\"source_id\":null,\"status\":\"CLOSED\",\"taxes\":[{\"id\":\"6ecd4ad7-056e-11e8-adec-0afa8de11450\",\"name\":\"GST\",\"rate\":\"0.15000\",\"tax\":98.8826}],\"totals\":{\"total_loyalty\":\"0.00000\",\"total_payment\":\"758.10000\",\"total_price\":\"659.21740\",\"total_tax\":\"98.88260\",\"total_to_pay\":\"0.00000\"},\"updated_at\":\"2019-09-26T14:54:37+00:00\",\"user\":{\"created_at\":\"2019-08-11 22:27:13\",\"display_name\":\"Vivek George\",\"email\":\"geovi616@student.otago.ac.nz\",\"id\":\"06c2f1bf-e97c-11e9-efcf-bc872a76c4fd\",\"name\":\"geovi616\",\"target_daily\":null,\"target_monthly\":null,\"target_weekly\":null,\"updated_at\":\"2019-08-11 23:04:55\"},\"user_id\":\"06c2f1bf-e97c-11e9-efcf-bc872a76c4fd\",\"version\":12050530028}";

		// create a mock server than can receive (SMTP) and host (IMAP)
		GreenMail greenMail = new GreenMail(ServerSetupTest.SMTP_IMAP);

		// add a user
		greenMail.setUser("test@localhost", "test@localhost", "password");

		// start the server
		greenMail.start();

		System.out.println("IMAP Server Ready on port " + greenMail.getImap().getPort());
		System.out.println("Press Enter to send an E-Mail...");

		// a loop for sending messages
		while (true) {
			System.in.read();  // wait for user to hit enter
			GreenMailUtil.sendTextEmailTest("test@localhost", "test@localhost", subject, body);
			System.out.println(" sent.");
			System.out.print("Press Enter to send an E-Mail...");
		}

	}
}
